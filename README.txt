﻿
The data sets used in the experiments section of the Validity Index-based Fuzzy ART paper are available at:

UCI machine learning repository: http://archive.ics.uci.edu/ml

The "main_example.m" file contains an example of usage of the CVI-Fuzzy-ART code.